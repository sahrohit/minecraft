auto_commit.sh
=======================================
#!/bin/bash
MESSAGE="Auto-commit: $(date)"
REPO_PATH="/home/azureuser"
git -C "$REPO_PATH" add -A
git -C "$REPO_PATH" commit -m "$MESSAGE"
git -C "$REPO_PATH" push -u origin main
